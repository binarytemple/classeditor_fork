<!--

// Author: Tanmay K. Mohapatra

var activeArr = new Array();
var pathFromRoot;
activeArr[0] = "1";

function setMenuPosition(activeArrIn, pathFromRootIn)
{
	activeArr = activeArrIn;
        pathFromRoot = pathFromRootIn;
}

function printFullMenu()
{
    with(document)
    {
	var iLevel = 0;
	
	// Home
	printMenuItem("Home", pathFromRoot + "index.html", null, "ClassEditor Home", (0 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");
	
	// Project Page
	printMenuItem("Project Page", "http://sourceforge.net/projects/classeditor/", null, "ClassEditor Project Page", (1 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");
	
	// Download
	printMenuItem("Download", "http://sourceforge.net/project/showfiles.php?group_id=89483", null, "Classeditor Download", (2 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");
	
	// Screen Shots
	printMenuItem("Screen Shots", pathFromRoot + "screenshot/index.html", null, "Classeditor Screen Shots", (3 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");

	// Release History
	printMenuItem("Release History", pathFromRoot + "history.html", null, "Classeditor Release History", (4 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");

	// Forums
	printMenuItem("Forums", "http://sourceforge.net/forum/?group_id=89483", null, "Classeditor Forums", (5 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");

	// Feature Request
	printMenuItem("Feature Request", "http://sourceforge.net/tracker/?atid=590273&group_id=89483&func=browse", null, "Classeditor Feature Request", (6 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");

	// Submit Bug
	printMenuItem("Submit Bug", "http://sourceforge.net/tracker/?atid=590270&group_id=89483&func=browse", null, "Classeditor Bug Submission", (7 == activeArr[iLevel]), iLevel);
	write("<img src='" + pathFromRoot + "images/green_pix.png' width='200' height='1'/><br>");
    }
}

function printMenuItem(itemName, itemUrl, itemTarget, itemDesc, isActive, iLevel)
{
    with(document)
    {
	var iIndex;
	
	// start off the font tag
	//write("<font size='-1'>");

	// write the link	
	write("<a href='" + itemUrl + "' " +
		"class='" + (isActive ? "activemenu" : "inactivemenu") + "' " +
		"title='" + itemDesc + "' " +
		"alt='" + itemName + "' ");
	if(null != itemTarget)
	{
		write("target='" + itmTarget + "' ");
	}
	write(">");
	
	// indent the item
	for(iIndex=0; iIndex < iLevel;  iIndex++)
	{
		write(" &nbsp; ");
	}
	// write the name
	write((isActive ? "- " : "+ ") + itemName);
	write("</a>");
	// end the font tag
	//write("</font>");
        write("<br>");
    }
}

function printTopBar(pathFromRoot)
{
    with(document)
    {
        write('<center>');
        write('<img height=50 src='+pathFromRoot+'images/classeditor.gif width=50> &nbsp; ');
        write('<font color=#ffff00 size=+3> Java Class File Editor</font>');
        write('</center>');
    }
}

function printTrailer()
{
    with(document)
    {
        write('<hr noshade width="80%">');
    }
}
//-->

